![gallium framework logo](./gallium-framework.png)

# Gallium Framework

**Gallium Framework** is a coding framework built for the new web, but like the old web. It allows creation of web apps without touching existing web technologies.

## Structure
The web structure is directly built using [YAML](https://yaml.org/). This allows for predefined components and user-defined components, while allowing fast prototyping and localization support.

## Logic
Logic is written in [Pellet](./PELLET.md) and can be reactive, the Gallium Framework allows simple and optional code for each component on the website.

## Designs
The Gallium Framework allows for various default designs, these are grouped into 3 categories:
 - `dark` *(dark mode only)*
 - `classic` *(light mode only)*
 - `user` *(adjusts to user colorscheme)*
 - `oled` *(pure white/back combination for OLED screens)*

By default we include 3 designs:
 - `snack`
 - `tapas`
 - `starter`

There is no *"superior"* design, the three designs are equally great and they adapt to the developer.
You may prefer `snack` and someone else may prefer `starter`.
For starting, we recommend the `tapas-user` design-variant pair, as `tapas` is the one that was developed the most.
