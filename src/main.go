package main

import (
	"errors"
	"fmt"
	"io"
	"io/fs"
	"os"
	"path/filepath"
	"reflect"
	"strconv"
	"strings"

	"github.com/gomarkdown/markdown"
	"github.com/gomarkdown/markdown/parser"
	"gopkg.in/yaml.v3"
)

func tostr(i interface{}) string {
  typ := reflect.TypeOf(i).String()
  if typ == "string" {
    return i.(string)
  } else if typ == "int" {
    ii := i.(int)
    return strconv.Itoa(ii)
  } else if typ == "float64" {
    fl := i.(float64)
    return strconv.FormatFloat(fl, 'f', -1, 64)
  } else if typ == "float32" {
    fl := i.(float32)
    return strconv.FormatFloat(float64(fl), 'f', -1, 32)
  } else if typ == "bool" {
    if i.(bool) {
      return "true"
    }
    return "false"
  }
  return "nil"
}

func sanitize(s string) string {
  return strings.ReplaceAll(strings.ReplaceAll(strings.ReplaceAll(s, `"`, "&quot;"), "<", "&lt;"), ">", "&gt;")
}

func buildComponent(path string, compname string, basedir string, outdir string) string {
  f, err := os.Open(path)
  if err != nil {
    panic(err)
  }

  direct := false
  built := ""
  
  d := yaml.NewDecoder(f)
  for {
    spec := make(map[string]interface{})
    err := d.Decode(&spec)
    if spec == nil {
      continue
    }

    if errors.Is(err, io.EOF) {
      break
    }

    if err != nil {
      f.Close()
      panic(err)
    }

    if val, ok := spec["content"]; ok {
      built = buildHTML(val.([]interface{}))
    }
    if val, ok := spec["metadata"]; ok {
      if stt, ok := val.(map[string]interface{})["static"]; ok {
        for _, itm := range stt.([]interface{}) {
          copyStatic(basedir + "/" + itm.(string), basedir, outdir)
        }
      }
      if dirct, ok := val.(map[string]interface{})["direct"]; ok {
        if dirct.(bool) {
          direct = true
        }
      }
    }
  }

  f.Close()
  if direct {
    return built
  }
  return `<div data-galliumcmp class="galliumcmp-` + compname + `">` + built + "</div>"
}

func mdparse(md string) string {
  mdparser := parser.NewWithExtensions(parser.CommonExtensions | parser.AutoHeadingIDs)
  html := string(markdown.ToHTML([]byte(md), mdparser, nil))
  html = strings.Trim(html, " \t\n")
  return strings.TrimSuffix(strings.TrimPrefix(html, "<p>"), "</p>")
}

func buildHTML(spec []interface{}) string {
  built := ""

  for _, elmv := range spec {
    elm := elmv.(map[string]interface{})
    if len(elm) == 1 {
      // we loop over the only element in the map, because yes
      for k, v := range elm {
        if _, ok := componentdb[k]; !ok {
          panic(fmt.Errorf("component '%s' not found", k))
        }
        built += strings.ReplaceAll(strings.ReplaceAll(componentdb[k](make(map[string]string)), "{{content}}", mdparse(tostr(v))), "{{attr}}", "")
      }
    } else {
      ctyp := "div"
      attrsa := make(map[string]string)
      attrs := " "
      content := ""
      // we have to load every attribute + element type + content/children
      for k, v := range elm {
        if v == nil {
          ctyp = k
        } else if k == "content" {
          content = mdparse(tostr(v))
        } else if k == "children" {
          content = buildHTML(v.([]interface{}))
        } else {
          attrs += k + `="` + sanitize(tostr(v)) + `" `
          attrsa[k] = mdparse(tostr(v))
        }
      }
      if _, ok := componentdb[ctyp]; !ok {
        panic(fmt.Errorf("component '%s' not found", ctyp))
      }
      tobuild := strings.ReplaceAll(strings.ReplaceAll(componentdb[ctyp](attrsa), "{{content}}", content), "{{attr}}", strings.TrimRight(attrs, " "))
      for k, v := range attrsa {
        tobuild = strings.ReplaceAll(tobuild, "{{" + k + "}}", v)
      }
      built += tobuild
    }
    built += "\n"
  }

  return built
}

func copyStatic(path string, basedir string, outdir string) {
  outpath := strings.ReplaceAll(path, basedir, outdir)
  r, err := os.Open(path)
  if err != nil { panic(err) }
  defer r.Close()

  err = os.MkdirAll(filepath.Dir(outpath), 0777)
  if err != nil { panic(err) }

  w, err := os.OpenFile(outpath, os.O_RDWR | os.O_CREATE, 0666)
  if err != nil { panic(err) }
  defer w.Close()

  _, err = io.Copy(w, r)
  if err != nil { panic(err) }
}

func buildPage(path string, basedir string, outdir string) string {
  f, err := os.Open(path)
  if err != nil {
    panic(err)
  }

  needinjection := false
  injscript := `const comp = {}
const compevt = {}
function argsof(func) {
  const pattern = /[^(]*\(([^)]*)\)/;
  return func.toString().match(pattern)[1].split(/,\s*/);
}
function onComponentRender(name, fnc) { comp[name] = fnc }
function addComponentEvents(name, fnc) { compevt[name] = fnc }
function renderComponent(name, props={}) {
  let outargs = []
  const trueargs = argsof(comp[name])
  for (const arg of trueargs) {
    outargs.push(props[arg])
  }
  return comp[name](...outargs)
}
` + PelletSTDJavaScript()

  desc := "a website built using gallium framework"
  design := "starter-user"
  lang := "en"
  built := `<!DOCTYPE html>
<html lang="<!-- lang -->">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="<!-- description -->">
<!-- design -->
<!-- head -->
</head>
<body>
<!-- body -->
<!-- scripts -->
<script><!-- inject --></script>
</body>
</html>`

  d := yaml.NewDecoder(f)
  for {
    spec := make(map[string]interface{})
    err := d.Decode(&spec)
    if spec == nil {
      continue
    }

    if errors.Is(err, io.EOF) {
      break
    }

    if err != nil {
      f.Close()
      panic(err)
    }

    if val, ok := spec["metadata"]; ok {
      if dsg, ok := val.(map[string]interface{})["design"]; ok {
        design = dsg.(string)
      }
      if lng, ok := val.(map[string]interface{})["lang"]; ok {
        lang = sanitize(lng.(string))
      }
      if dsc, ok := val.(map[string]interface{})["description"]; ok {
        desc = sanitize(dsc.(string))
      }

      if req, ok := val.(map[string]interface{})["require"]; ok {
        for _, itm := range req.([]interface{}) {
          namspl := strings.Split(itm.(string), "/")
          nam := namspl[len(namspl)-1]
          if _, ok := componentdb[nam]; !ok {
            if _, err := os.Stat(basedir + "/" + itm.(string) + ".plt"); err == nil {
              builtcmp, js := BuildPelletComponent(basedir + "/" + itm.(string) + ".plt", nam)
              injscript += js
              needinjection = true
              componentdb[nam] = builtcmp
            } else {
              componentdb[nam] = MakeComponentBuilder(buildComponent(basedir + "/" + itm.(string) + ".yml", nam, basedir, outdir))
              
            }
          }
        }
      }

      if lnk, ok := val.(map[string]interface{})["links"]; ok {
        for _, itm := range lnk.([]interface{}) {
          if strings.HasSuffix(itm.(string), "/") {
            isrootpath := true
            filepath.WalkDir(basedir + "/" + itm.(string), func(path string, d fs.DirEntry, err error) error {
              if d.IsDir() {
                if isrootpath {
                  isrootpath = false
                  return nil
                }
                return fs.SkipDir
              }

              if strings.HasSuffix(d.Name(), ".yml") {
                buildPage("./" + path, basedir, outdir)
              }

              return nil
            })
          } else {
            buildPage(basedir + "/" + itm.(string) + ".yml", basedir, outdir)
          }
        }
      }

      if scr, ok := val.(map[string]interface{})["scripts"]; ok {
        scrlines := ""
        for _, itm := range scr.([]interface{}) {
          scrlines += `<script src="` + itm.(string) + `"></script>` + "\n"
        }
        built = strings.ReplaceAll(built, "<!-- scripts -->", scrlines)
      }

      if stt, ok := val.(map[string]interface{})["static"]; ok {
        for _, itm := range stt.([]interface{}) {
          copyStatic(basedir + "/" + itm.(string), basedir, outdir)
        }
      }

      built = strings.ReplaceAll(built, "<!-- design -->", "<style>" + designdb[design] + "</style>")
    }
    if val, ok := spec["head"]; ok {
      built = strings.ReplaceAll(built, "<!-- head -->", buildHTML(val.([]interface{})))
    }
    if val, ok := spec["body"]; ok {
      built = strings.ReplaceAll(built, "<!-- body -->", buildHTML(val.([]interface{})))
    }
  }
  built = strings.ReplaceAll(built, "<!-- lang -->", lang)
  built = strings.ReplaceAll(built, "<!-- description -->", desc)

  if needinjection {
    injscript += `for (const name in comp) {
  const elems = document.querySelectorAll(".galliumcmp-"+name)
  for (const elem of elems) {
    const observer = new MutationObserver(list => {
      let found = false
      for (const rec of list) {
        if (rec.attributeName.startsWith("data-")) {
          found = true
          break
        }
      }
      if (found) {
        elem.innerHTML = renderComponent(name, elem.dataset)
        if (name in compevt) {
          compevt[name](elem)
        }
      }
    })
    observer.observe(elem, { attributes: true })
    if (name in compevt) {
      compevt[name](elem)
    }
  }
}`
    built = strings.ReplaceAll(built, "<!-- inject -->", injscript)
  }

  f.Close()
  outpath := strings.TrimSuffix(strings.ReplaceAll(path, basedir, outdir), ".yml") + ".html"

  err = os.MkdirAll(filepath.Dir(outpath), 0777)
  if err != nil { panic(err) }

  err = os.WriteFile(outpath, []byte(built), 0666)
  if err != nil {
    panic(err)
  }
  return built
}

func main() {
  args := os.Args[1:]

  if len(args) != 2 {
    fmt.Printf("usage: galliumframework <input-dir> <output-dir>\n")
    os.Exit(1)
  }

  InitDesignDB()
  InitComponentDB()

  basedir := args[0]
  outdir := args[1]

  buildPage(basedir + "/index.yml", basedir, outdir)
  fmt.Println("done")
}
