package main

import (
	"fmt"
	"reflect"
	"regexp"
	"strconv"
)

func PelletSTDstrfmt(s string, r ...interface{}) string {
  ret := s
  for i := 0; i < len(r); i++ {
    re := regexp.MustCompile(`\{` + fmt.Sprint(i) + `\}`)
    ret = re.ReplaceAllString(ret, fmt.Sprintf("%v", r[i]))
  }
  return ret
}

func PelletSTDJavaScript() string {
  return `
const std={strfmt(f){let o=f;for(let i=1;i<arguments.length;i++){o=o.replace(new RegExp('\\{'+(i-1)+'\\}','gi'),arguments[i]);}return o}};
const number=(s)=>Number.isFinite(s=parseFloat(s))?s:0;
const json=(s)=>JSON.parse(s)
const string=(x)=>(typeof x=='object')?JSON.stringify(x):x.toString()
`
}

type PelletSTDEnv struct {
  vars map[string]PelletAST
  returned bool
  parent *PelletSTDEnv
}

func (env *PelletSTDEnv) AddFunction(name string, fnc func([]PelletAST, *PelletSTDEnv) PelletAST) {
  env.vars[name] = fnc
}
func (env *PelletSTDEnv) SetLocal(sym string, val PelletAST) {
  env.vars[sym] = val
}
func (env *PelletSTDEnv) Set(sym string, val PelletAST) bool {
  if val, ok := env.vars[sym]; ok {
    env.vars[sym] = val
    return true
  }
  if env.parent != nil {
    if env.parent.Set(sym, val) {
      return true
    }
  }
  env.vars[sym] = val
  return true
}
func (env *PelletSTDEnv) Get(sym string) PelletAST {
  if val, ok := env.vars[sym]; ok {
    return val
  }
  if env.parent != nil {
    return env.parent.Get(sym)
  }
  return nil
}
func PelletSTDMakeEnv() *PelletSTDEnv {
  env := PelletSTDEnv{}
  env.vars = make(map[string]PelletAST)
  env.returned = false

  env.AddFunction("std", func(in []PelletAST, env *PelletSTDEnv) PelletAST {
    if len(in) < 1 { return nil }
    prop := PelletSTDEval(in[0], env).(PelletASTExpr).val.(string)
    if prop == "strfmt" {
      return func(in []PelletAST, env *PelletSTDEnv) PelletAST {
        if len(in) < 1 { return nil }
        fmt := PelletSTDConvertTypeString(PelletSTDEval(in[0], env).(PelletASTExpr), env)
        vals := make([]interface{}, 0)
        for i := 1; i < len(in); i++ {
          vals = append(vals, PelletSTDConvertTypeString(PelletSTDEval(in[i], env).(PelletASTExpr), env))
        }
        
        return PelletASTExpr{ typ: PelletTokStr, val: PelletSTDstrfmt(fmt, vals...) }
      }
    }
    return PelletASTExpr{}
  })
  env.AddFunction("number", func(in []PelletAST, env *PelletSTDEnv) PelletAST {
    if len(in) < 1 { return nil }
    return PelletASTExpr{ typ: PelletTokNum, val: PelletSTDConvertTypeNumber(PelletSTDEval(in[0], env).(PelletASTExpr), env) }
  })
  env.AddFunction("string", func(in []PelletAST, env *PelletSTDEnv) PelletAST {
    if len(in) < 1 { return nil }
    return PelletASTExpr{ typ: PelletTokStr, val: PelletSTDConvertTypeString(PelletSTDEval(in[0], env).(PelletASTExpr), env) }
  })

  // TODO The `json(x)` function in Pellet

  env.AddFunction("if", func(in []PelletAST, env *PelletSTDEnv) PelletAST {
    if len(in) < 2 { return nil }
    condition := PelletSTDConvertTypeNumber(PelletSTDEval(in[0], env).(PelletASTExpr), env)
    if condition != 0 {
      newenv := PelletSTDMakeEnv()
      newenv.parent = env
      vfunc := in[1].(PelletASTFunc)
      return PelletSTDExec(vfunc.body, newenv)
    } else if (len(in) == 3) {
      newenv := PelletSTDMakeEnv()
      newenv.parent = env
      vfunc := in[2].(PelletASTFunc)
      return PelletSTDExec(vfunc.body, newenv)
    }

    return nil
  })
  env.AddFunction("eq", func(in []PelletAST, env *PelletSTDEnv) PelletAST {
    if len(in) != 2 { return nil }
    n1 := PelletSTDConvertTypeString(PelletSTDEval(in[0], env).(PelletASTExpr), env)
    n2 := PelletSTDConvertTypeString(PelletSTDEval(in[1], env).(PelletASTExpr), env)
    if n1 == n2 {
      return PelletASTExpr{ typ: PelletTokNum, val: "1" }
    }
    return PelletASTExpr{ typ: PelletTokNum, val: "0" }
  })
  env.AddFunction("not", func(in []PelletAST, env *PelletSTDEnv) PelletAST {
    if len(in) != 1 { return nil }
    n := PelletSTDConvertTypeNumber(PelletSTDEval(in[0], env).(PelletASTExpr), env)
    if n == 0 {
      return PelletASTExpr{ typ: PelletTokNum, val: "1" }
    }
    return PelletASTExpr{ typ: PelletTokNum, val: "0" }
  })
  env.AddFunction("switch", func(in []PelletAST, env *PelletSTDEnv) PelletAST {
    i := 0
    for i < len(in) {
      if i == len(in) - 1 {
        newenv := PelletSTDMakeEnv()
        newenv.parent = env
        vfunc := in[i].(PelletASTFunc)
        return PelletSTDExec(vfunc.body, newenv)
      }
      condition := PelletSTDConvertTypeNumber(PelletSTDEval(in[i], env).(PelletASTExpr), env)
      i += 1
      if condition != 0 {
        newenv := PelletSTDMakeEnv()
        newenv.parent = env
        vfunc := in[i].(PelletASTFunc)
        return PelletSTDExec(vfunc.body, newenv)
      }
      i += 1
    }
    return nil
  })

  return &env
}

func PelletSTDConvertTypeNumber(expr PelletASTExpr, env *PelletSTDEnv) float64 {
  if expr.typ == PelletTokNum {
    f, _ := strconv.ParseFloat(expr.val.(string), 32)
    return f
  } else if expr.typ == PelletTokStr {
    f, _ := strconv.ParseFloat(expr.val.(string), 32)
    return f
  } else if expr.typ == PelletTokOpr {
    return 0
  } else if expr.typ == PelletTokSym {
    return PelletSTDConvertTypeNumber(PelletSTDEval(expr, env).(PelletASTExpr), env)
  }
  return 0
}
func PelletSTDConvertTypeString(expr PelletASTExpr, env *PelletSTDEnv) string {
  if expr.typ == PelletTokNum {
    if reflect.TypeOf(expr.val).Kind() == reflect.Float64 {
      return strconv.FormatFloat(expr.val.(float64), 'f', -1, 32)
    }
    return expr.val.(string)
  } else if expr.typ == PelletTokStr {
    return expr.val.(string)
  } else if expr.typ == PelletTokOpr {
    return expr.val.(string)
  } else if expr.typ == PelletTokSym {
    return PelletSTDConvertTypeString(PelletSTDEval(expr, env).(PelletASTExpr), env)
  }
  return ""
}
func PelletSTDEval(ast PelletAST, env *PelletSTDEnv) PelletAST {
  switch v := ast.(type) {
  case PelletASTExpr:
    if v.typ == PelletTokSym {
      return PelletSTDEval(env.Get(v.val.(string)), env)
    }
    return v
  case PelletASTOper:
    left := PelletSTDEval(v.left, env).(PelletASTExpr)
    right := PelletSTDEval(v.right, env).(PelletASTExpr)
    if left.typ == PelletTokStr && right.typ == PelletTokStr {
      if v.opr != "+" {
        panic(fmt.Errorf("unknown string operation, '%s' %s '%s'", left.val.(string), v.opr, right.val.(string)))
      }
      return PelletASTExpr{ typ: PelletTokStr, val: left.val.(string) + right.val.(string) }
    } else if left.typ == PelletTokNum && right.typ == PelletTokNum {
      leftv, _ := strconv.ParseFloat(left.val.(string), 32)
      rightv, _ := strconv.ParseFloat(right.val.(string), 32)
      switch v.opr {
      case "+":
        return PelletASTExpr{ typ: PelletTokNum, val: strconv.FormatFloat(leftv + rightv, 'f', -1, 32) }
      case "-":
        return PelletASTExpr{ typ: PelletTokNum, val: strconv.FormatFloat(leftv - rightv, 'f', -1, 32) }
      case "*":
        return PelletASTExpr{ typ: PelletTokNum, val: strconv.FormatFloat(leftv * rightv, 'f', -1, 32) }
      case "/":
        return PelletASTExpr{ typ: PelletTokNum, val: strconv.FormatFloat(leftv / rightv, 'f', -1, 32) }
      default:
        panic(fmt.Errorf("unknown number operation, %f %s %f", leftv, v.opr, rightv))
      }
    } else {
      panic(fmt.Errorf("unknown operation type combination, '%s' %s '%s'", left, v.opr, right))
    }
  case PelletASTAsgn:
    val := PelletSTDEval(v.val, env)
    env.Set(v.sym, val)
    return val
  case PelletASTCall:
    var vcall PelletAST
    if reflect.TypeOf(v.sym) == reflect.TypeOf(PelletASTExpr{}) &&
    v.sym.(PelletASTExpr).typ == PelletTokSym &&
    v.sym.(PelletASTExpr).val.(string) == "return" {
      env.returned = true
      return PelletSTDEval(v.args[0], env)
    } else if reflect.TypeOf(v.sym) == reflect.TypeOf(PelletASTExpr{}) &&
    v.sym.(PelletASTExpr).typ == PelletTokSym {
      vcall = env.Get(v.sym.(PelletASTExpr).val.(string))
    } else {
      vcall = PelletSTDEval(v.sym, env)
    }
    if reflect.TypeOf(vcall) == reflect.TypeOf(PelletASTFunc{}) {
      // functions defined inside Pellet
      newenv := PelletSTDMakeEnv()
      newenv.parent = env
      vfunc := vcall.(PelletASTFunc)
      for i, arg := range vfunc.params {
        argexp := arg.(PelletASTExpr)
        newenv.SetLocal(argexp.val.(string), v.args[i])
      }
      return PelletSTDExec(vfunc.body, newenv)
    }
    // functions defined outside Pellet
    if vcall == nil { panic(fmt.Errorf("attempted to call a nil value")) }
    fn := vcall.(func([]PelletAST, *PelletSTDEnv) PelletAST)
    return fn(v.args, env)
  case PelletASTFunc:
    return v
  case nil:
    return PelletASTExpr{ typ: PelletTokNum, val: "0" }
  default:
    panic(fmt.Errorf("cannot evaluate '%s'", v))
  }
}

func PelletSTDExec(ast []PelletAST, env *PelletSTDEnv) PelletAST {
  for _, a := range ast {
    r := PelletSTDEval(a, env)
    if env.returned {
      return r
    }
  }

  return PelletASTExpr{}
}
