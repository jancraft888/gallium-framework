package main

import "fmt"

var designdb map[string]string = make(map[string]string)
type Design func(variant string) string

func VariantByName(name string) string {
  if name == "dark" {
    return `:root{--bg:#222;--fg:#eee;--sf:#444;--sf2:#333;--bd:#777;color-scheme:dark}`
  } else if name == "classic" {
    return `:root{--bg:#eee;--fg:#222;--sf:#aaa;--sf2:#999;--bd:#eee;color-scheme:light}`
  } else if name == "user" {
    return `:root{--bg:#222;--fg:#eee;--sf:#444;--sf2:#333;--bd:#777}@media(prefers-color-scheme:light){:root{--bg:#eee;--fg:#222;--sf:#aaa;--sf2:#999;--bd:#eee}}`
  } else if name == "oled" {
    return `:root{--bg:#000;--fg:#fff;--sf:#333;--sf2:#222;--bd:#666;color-scheme:dark}`
  } else {
    panic(fmt.Errorf("unknown variant: %s", name))
  }
}

func SnackDesign(variant string) string {
  return VariantByName(variant) + `html{font-family:monospace;display:flex;justify-content:center;background:var(--bg);color:var(--fg)}body{width:80ch}img,video,audio{display:none}code{padding:.1rem .3rem;overflow-x:scroll;background:var(--sf)}
a{color:inherit}button{cursor:pointer;border:none}
input,select,textarea{box-sizing:border-box;width:100%;padding:1rem;resize:vertical}input[type=submit],input[type=reset],input[type=button]{cursor:pointer}`
}

func TapasDesign(variant string) string {
  return VariantByName(variant) + `html{font-family:monospace;display:flex;justify-content:center;background:var(--bg);color:var(--fg)}body{width:80ch}img{float:left;image-rendering:pixelated}code{padding:.1rem .3rem;border-radius:.25vh;overflow-x:scroll;background:var(--sf)}h1,h2{text-align:center}h1+h2{margin-top:0}h1:has(+h2){margin-bottom:.25rem}
table{border-collapse:collapse;border:1px solid var(--bd)}th{padding:.5rem;border-bottom:2px solid var(--bd)}td{padding:.5rem;border:1px solid var(--bd)}thead,tfoot{background:var(--sf)}tbody{background:var(--sf2)}
a,button{text-decoration:none;color:var(--bg);background:var(--fg);padding:.075rem .3rem;border:none}button{padding:.5rem;cursor:pointer}
input,select,textarea{color:var(--fg);background:var(--sf);box-sizing:border-box;width:100%;padding:1rem;resize:vertical;border:none;margin:.5rem;outline:none;border:1px solid var(--sf2)}input:focus-visible,select:focus-visible,textarea:focus-visible{border:1px solid var(--fg);}input[type=submit],input[type=reset],input[type=button]{cursor:pointer;background:var(--sf2)}
`
}

func StarterDesign(variant string) string {
  return VariantByName(variant) + `html{font-family:sans-serif;display:flex;justify-content:center;background:var(--bg);color:var(--fg)}body{width:120ch}img{float:left}code{padding:.1rem .3rem;border-radius:.25vh;overflow-x:scroll;background:var(--sf)}h1,h2{text-align:center}h1+h2{margin-top:0}h1:has(+h2){margin-bottom:.25rem}
th,td{padding:.5rem}table{border-collapse:collapse;border:1px solid var(--bd)}tfoot{border-top:1px solid var(--bd)}tr{background:var(--sf2)}tbody tr:nth-child(odd){background:var(--sf)}
a,button{text-decoration:none;color:var(--bg);background:var(--fg);padding:.075rem .3rem;border:none}button{padding:.5rem;cursor:pointer}
input,select,textarea{box-sizing:border-box;width:100%;padding:1rem;resize:vertical;border-radius:1vh;border:1px solid var(--bd);margin:.5rem;outline:none}input:focus-visible,select:focus-visible,textarea:focus-visible{border:1.5px solid var(--fg)}
`
}

func InitDesignDB() {
  designs := map[string]Design{}
  designs["snack"] = SnackDesign
  designs["tapas"] = TapasDesign
  designs["starter"] = StarterDesign

  variants := []string{ "classic", "dark", "user", "oled" }

  for dname, dfunc := range designs {
    for _, vstr := range variants {
      designdb[dname + "-" + vstr] = dfunc(vstr)
    }
  }
}
