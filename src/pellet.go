package main

import (
	"fmt"
	"reflect"
	"regexp"
	"strings"
)

func RegExpMatch(pattern string, s string) bool {
  ok, err := regexp.Match(pattern, []byte(s))
  return ok && err == nil
}

type Iterator[T comparable] struct {
  arr []T
  next T
  isNext bool
  idx int
}
func (i *Iterator[T]) _Fill() {
  if i.idx >= len(i.arr) {
    var zero T // we have to create a nil instance by force
    i.next = zero
    i.isNext = false
    return
  }
  i.next = i.arr[i.idx]
  i.idx += 1
}
func (i *Iterator[T]) MoveNext() T {
  ret := i.next
  i._Fill()
  return ret
}
func MakeIterator[T comparable](arr []T) *Iterator[T] {
  iter := Iterator[T]{}
  iter.arr = arr
  iter.idx = 0
  iter.isNext = true
  iter._Fill()
  return &iter
}

const (
  PelletTokOpr  string = "opr"
  PelletTokStr         = "str"
  PelletTokNum         = "num"
  PelletTokSym         = "sym"
)

type PelletToken struct {
  typ string
  dat string
}

type PelletAST interface{}
type PelletASTExpr struct {
  PelletAST
  typ string
  val interface{}
}
type PelletASTOper struct {
  PelletAST
  opr string
  left PelletAST
  right PelletAST
}
type PelletASTCall struct {
  PelletAST
  sym PelletAST
  args []PelletAST
  dotsyntx bool
}
type PelletASTFunc struct {
  PelletAST
  params []PelletAST
  body []PelletAST
}
type PelletASTAsgn struct {
  PelletAST
  sym string
  val PelletAST
}
func (a PelletASTExpr) String() string {
  return fmt.Sprintf("{%s %s}", a.typ, a.val)
}
func (a PelletASTOper) String() string {
  return fmt.Sprintf("{oper %s %s %s}", a.left, a.opr, a.right)
}
func (a PelletASTCall) String() string {
  return fmt.Sprintf("{call %s %s}", a.sym, a.args)
}
func (a PelletASTFunc) String() string {
  return fmt.Sprintf("{func %s %s}", a.params, a.body)
}
func (a PelletASTAsgn) String() string {
  return fmt.Sprintf("{asg %s = %s}", a.sym, a.val)
}

func _scan(fchar rune, chars *Iterator[rune], allowed string) string {
  ret := string(fchar)
  p := chars.next

  for chars.isNext && RegExpMatch(allowed, string(p)) {
    ret += string(chars.MoveNext())
    p = chars.next
  }
  return ret
}
func _scan_string(delim rune, chars *Iterator[rune]) string {
  ret := ""
  for chars.next != delim {
    if !chars.isNext {
      panic(fmt.Errorf("String ran off the EOF"))
    }
    c := chars.MoveNext()
    if c == '\\' {
      c = chars.MoveNext()
    }
    ret += string(c)
  }
  chars.MoveNext()
  return ret
}

type PelletParser struct {
  tokens *Iterator[PelletToken]
  stop_at string
}
func (self *PelletParser) FailIfAtEnd(expected string) {
  if !self.tokens.isNext {
    panic(fmt.Errorf("hit EOF - expected '%s'", expected))
  }
}
func (self *PelletParser) MultipleExpressions(sep string, end string) []PelletAST {
  ret := make([]PelletAST, 0)
  self.FailIfAtEnd(end)
  typ := self.tokens.next.typ
  if typ == end {
    self.tokens.MoveNext()
  } else {
    arg_parser := PelletParser{ self.tokens, sep + end }
    for typ != end {
      p := arg_parser.NextExpression(nil)
      if p != nil {
        ret = append(ret, p)
      }
      typ = self.tokens.next.typ
      self.tokens.MoveNext()
      self.FailIfAtEnd(end)
    }
  }
  return ret
}
func (self *PelletParser) ParameterList() []PelletAST {
  if self.tokens.next.typ != ":" {
    return []PelletAST{}
  }
  self.tokens.MoveNext()
  typ := self.tokens.next.typ
  if typ != "(" {
    panic(fmt.Errorf("':' must be followed by '(' in a function"))
  }
  self.tokens.MoveNext()
  ret := self.MultipleExpressions(",", ")")
  for _, param := range ret {
    if param.(PelletASTExpr).typ != PelletTokSym {
      panic(fmt.Errorf("only symbols are allowed in function parameter lists, found '%s'", param))
    }
  }
  return ret
}
func (self *PelletParser) NextExpression(prev PelletAST) PelletAST {
  self.FailIfAtEnd(";")
  tok := self.tokens.next
  typ := tok.typ
  dat := tok.dat

  if strings.Contains(self.stop_at, tok.typ) { return prev }
  self.tokens.MoveNext()
  if (typ == PelletTokNum || typ == PelletTokStr || typ == PelletTokSym) && prev == nil {
    return self.NextExpression(PelletASTExpr{ typ: typ, val: dat })
  } else if typ == PelletTokOpr {
    nxt := self.NextExpression(nil)
    return self.NextExpression(PelletASTOper{ opr: dat, left: prev, right: nxt })
  } else if typ == "(" {
    args := self.MultipleExpressions(",", ")")
    return self.NextExpression(PelletASTCall{ sym: prev, args: args })
  } else if typ == "{" {
    params := self.ParameterList()
    body := self.MultipleExpressions(";", "}")
    return self.NextExpression(PelletASTFunc{ params: params, body: body })
  } else if typ == "=" {
    if reflect.TypeOf(prev) != reflect.TypeOf(PelletASTExpr{}) || prev.(PelletASTExpr).typ != PelletTokSym {
      panic(fmt.Errorf("attempted to assign something to a '%s', only symbols can be assigned", reflect.TypeOf(prev)))
    }
    nxt := self.NextExpression(nil)
    return self.NextExpression(PelletASTAsgn{ sym: prev.(PelletASTExpr).val.(string), val: nxt })
  } else if typ == PelletTokNum && dat == "." {
    nxt := self.tokens.MoveNext()
    return self.NextExpression(PelletASTCall{ sym: prev, args: []PelletAST{ PelletASTExpr{ typ: PelletTokStr, val: nxt.dat } }, dotsyntx: true })
  } else {
    panic(fmt.Errorf("unexpected token: {%s %s}", typ, dat))
  }
}

func PelletLex(inp string) []PelletToken {
  char_iter := MakeIterator([]rune(inp))
  output := make([]PelletToken, 0)

  for char_iter.isNext {
    c := char_iter.MoveNext()
    if strings.ContainsRune(" \t\n", c) {
      continue
    } else if strings.ContainsRune("(){},;=:", c) {
      output = append(output, PelletToken{ string(c), "" })
    } else if strings.ContainsRune("+-*/", c) {
      output = append(output, PelletToken{ PelletTokOpr, string(c) })
    } else if strings.ContainsRune("\"'", c) {
      output = append(output, PelletToken{ PelletTokStr, _scan_string(c, char_iter) })
    } else if RegExpMatch("[.0-9]", string(c)) {
      output = append(output, PelletToken{ PelletTokNum, _scan(c, char_iter, "[.0-9]") })
    } else if RegExpMatch("[_a-zA-Z]", string(c)) {
      output = append(output, PelletToken{ PelletTokSym, _scan(c, char_iter, "[_a-zA-Z]") })
    } else if c == '#' {
      _scan_string('\n', char_iter)
    } else {
      panic(fmt.Errorf("Unexpected character: %c", c))
    }
  }
  return output
}

// https://zetcode.com/golang/filter-map/
func ArrayMap[T, U any](data []T, f func(T) U) []U {
  res := make([]U, 0, len(data))
  for _, e := range data {
    res = append(res, f(e))
  }
  return res
}

func PelletParse(inp []PelletToken) []PelletAST {
  parser := PelletParser{ MakeIterator(inp), ";" }
  ast := make([]PelletAST, 0)
  for parser.tokens.isNext {
    p := parser.NextExpression(nil)
    if p != nil {
      ast = append(ast, p)
    }
    parser.tokens.MoveNext()
  }
  return ast
}

func PelletConvertSingleToJS(ast PelletAST) string {
  switch v := ast.(type) {
  case PelletASTExpr:
    if v.typ == PelletTokStr {
      return `"` + strings.ReplaceAll(strings.ReplaceAll(v.val.(string), "\n", "\\n"), `"`, `\"`) + `"`
    } else if v.typ == PelletTokNum || v.typ == PelletTokSym {
      return v.val.(string)
    } else {
      panic(fmt.Errorf("tried to convert expression of type '%s' to JS", v.typ))
    }
  case PelletASTOper:
    return PelletConvertSingleToJS(v.left) + v.opr + PelletConvertSingleToJS(v.right)
  case PelletASTAsgn:
    return v.sym + `=` + PelletConvertSingleToJS(v.val)
  case PelletASTCall:
    if v.dotsyntx {
      return PelletConvertSingleToJS(v.sym) + `.` + v.args[0].(PelletASTExpr).val.(string)
    }
    if len(v.args) == 2 &&
    reflect.TypeOf(v.args[0]) == reflect.TypeOf(PelletASTExpr{}) &&
    reflect.TypeOf(v.args[0].(PelletASTExpr).val).Kind() == reflect.String &&
    strings.HasPrefix(v.args[0].(PelletASTExpr).val.(string), "set ") {
      // assignment to a data structure property
      sym := strings.TrimPrefix(v.args[0].(PelletASTExpr).val.(string), "set ")
      return PelletConvertSingleToJS(v.sym) + `.` + sym + `=` + PelletConvertSingleToJS(v.args[1])
    }
    if reflect.TypeOf(v.sym) == reflect.TypeOf(PelletASTExpr{}) &&
    v.sym.(PelletASTExpr).typ == PelletTokSym &&
    v.sym.(PelletASTExpr).val.(string) == "if" {
      // ((condition) ? ((block1)()) : ((block2)()))
      condition := PelletConvertSingleToJS(v.args[0])
      block1 := ArrayMap(v.args[1].(PelletASTFunc).body, PelletConvertSingleToJS)
      ret := "((" + condition + ")?((()=>{" + strings.Join(block1, ";") + "})()):(("
      if len(v.args) == 3 {
        block2 := ArrayMap(v.args[2].(PelletASTFunc).body, PelletConvertSingleToJS)
        ret += "()=>{" + strings.Join(block2, ";") + "})()"
      } else {
        ret += "null)"
      }
      return ret + "))"
    }
    if reflect.TypeOf(v.sym) == reflect.TypeOf(PelletASTExpr{}) &&
    v.sym.(PelletASTExpr).typ == PelletTokSym &&
    v.sym.(PelletASTExpr).val.(string) == "eq" {
      // ((arg1) == (arg2))
      a1 := PelletConvertSingleToJS(v.args[0])
      a2 := PelletConvertSingleToJS(v.args[1])
      return "((" + a1 + ")==(" + a2 + "))"
    }
    if reflect.TypeOf(v.sym) == reflect.TypeOf(PelletASTExpr{}) &&
    v.sym.(PelletASTExpr).typ == PelletTokSym &&
    v.sym.(PelletASTExpr).val.(string) == "not" {
      // (!(arg))
      a := PelletConvertSingleToJS(v.args[0])
      return "(!(" + a + "))"
    }
    if reflect.TypeOf(v.sym) == reflect.TypeOf(PelletASTExpr{}) &&
    v.sym.(PelletASTExpr).typ == PelletTokSym &&
    v.sym.(PelletASTExpr).val.(string) == "switch" {
      // ((()=>{ if(condition){block}else if(condition){block}else {block} })())
      ret := "((()=>{"
      i := 0
      for i < len(v.args) {
        if i == len(v.args) - 1 {
          block := ArrayMap(v.args[i].(PelletASTFunc).body, PelletConvertSingleToJS)
          ret += "{" + strings.Join(block, ";") + "}"
          break
        }
        condition := PelletConvertSingleToJS(v.args[i])
        ret += "if(" + condition + "){"
        i += 1
        block := ArrayMap(v.args[i].(PelletASTFunc).body, PelletConvertSingleToJS)
        ret += strings.Join(block, ";") + "}"
        i += 1
        if i < len(v.args) { ret += "else " }
      }
      return ret + "})())"
    }
    args := ArrayMap(v.args, PelletConvertSingleToJS)
    return PelletConvertSingleToJS(v.sym) + `(` + strings.Join(args, ",") + `)`
  case PelletASTFunc:
    params := ArrayMap(v.params, PelletConvertSingleToJS)
    body := ArrayMap(v.body, PelletConvertSingleToJS)
    return `(` + strings.Join(params, ",") + `)=>{` + strings.Join(body, ";") + `}`
  default:
    panic(fmt.Errorf("cannot convert '%s' to JS", v))
  }
}

func PelletConvertToJS(ast []PelletAST, jsimpl string) string {
  ret := jsimpl + ";"
  if !strings.HasPrefix(jsimpl, `"nostd";`) {
    ret = PelletSTDJavaScript() + ";" + ret
  }

  for _, single := range ast {
    ret += PelletConvertSingleToJS(single) + ";"
  }

  return ret
}
