package main

import (
	"os"
	"strings"
)

var componentdb map[string]func(map[string]string)string = make(map[string]func(map[string]string)string)

func MakeComponentBuilder(comp string) func(map[string]string)string {
  return func(x map[string]string) string {
    return comp
  }
}
func InitComponentDB() {
  tags := []string{ "h1", "h2", "h3", "a", "p", "div", "title", "meta", "link", "img", "video", "audio", "style", "script", "table", "th", "tr", "td", "thead", "tbody", "tfoot", "colgroup", "col", "code", "pre", "form", "input", "select", "option", "label", "textarea", "b", "i", "ul", "ol", "li", "nav", "header", "footer", "main", "small" }
  for _, tag := range tags {
    componentdb[tag] = MakeComponentBuilder("<" + tag + "{{attr}}>{{content}}</" + tag + ">")
  }
  componentdb["br"] = MakeComponentBuilder("<br>")
  componentdb["hr"] = MakeComponentBuilder("<hr>")
}

func BuildPelletComponent(path string, name string) (func(map[string]string)string, string) {
  byt, err := os.ReadFile(path)
  if err != nil { panic(err) }
  str := string(byt)
  toks := PelletLex(str)
  ast := PelletParse(toks)
  js := PelletConvertToJS(ast, `"nostd";`)

  fnc := func(props map[string]string) string {
    env := PelletSTDMakeEnv()
    datattrs := ""
    for prop, val := range props {
      env.SetLocal(strings.TrimPrefix(prop, "data-"), PelletASTExpr{ typ: PelletTokStr, val: val })
      datattrs += prop + `="` + sanitize(val) + `" `
    }
    var ret PelletAST = nil
    env.AddFunction("onComponentRender", func(in []PelletAST, env *PelletSTDEnv) PelletAST {
      // onComponentRender(name, func)
      newenv := PelletSTDMakeEnv()
      newenv.parent = env
      ret = PelletSTDExec(in[1].(PelletASTFunc).body, newenv)
      return ret
    })
    env.AddFunction("renderComponent", func(in []PelletAST, env *PelletSTDEnv) PelletAST {
      // TODO Add full Component-in-Component (CiC) support
      return nil
    })
    env.AddFunction("addComponentEvents", func(in []PelletAST, env *PelletSTDEnv) PelletAST { return nil })
    PelletSTDExec(ast, env)
    return `<div data-galliumcmp class="galliumcmp-` + name + `" ` + datattrs + `>` + ret.(PelletASTExpr).val.(string) + `</div>`
  }

  return fnc, js
}
