onComponentRender("my-button", {:(count, rendered)
  html = "<div style='text-align: center'>
    <button>Button was clicked {0} times!</button><br>
    <small>and this component was rendered {1} times</small>
  </div>";
  return(std.strfmt(html, number(count), number(rendered)));
});

addComponentEvents("my-button", {:(element)
  element.querySelector("button").addEventListener("click", {
    dataset = element.dataset;
    dataset("set count", number(dataset.count) + 1);
    dataset("set rendered", number(dataset.rendered) + 1);
  });
});
