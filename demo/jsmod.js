// Gallium Framework JSmod (V1.0)

// TOP OF THE SCRIPT
const comp = {}
const compevt = {}
function argsof(func) {
  const pattern = /function[^(]*\(([^)]*)\)/;
  return func.toString().match(pattern)[1].split(/,\s*/);
}
function onComponentRender(name, fnc) { comp[name] = fnc }
function addComponentEvents(name, fnc) { comp[evt[name] = fnc] }
function renderComponent(name, props={}) {
  let outargs = []
  const trueargs = argsof(comp[name])
  for (const arg of trueargs) {
    outargs.push(props[arg])
  }
  return comp[name](...outargs)
}

// INJECT COMPONENT CODE HERE

// BOTTOM OF THE SCRIPT
for (const name in comp) {
  const elems = document.querySelectorAll(`.galliumcmp-${name}`)
  for (const elem of elems) {
    const observer = new MutationObserver(list => {
      let found = false
      for (const rec of list) {
        if (rec.attributeName.startsWith("data-")) {
          found = true
          break
        }
      }
      if (found) {
        elem.innerHTML = renderComponent(name, elem.dataset)
        if (name in compevt) {
          compevt[name](elem)
        }
      }
    })
    observer.observe(elem, { attributes: true })
    if (name in compevt) {
      compevt[name](elem)
    }
  }
}
